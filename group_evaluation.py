import os
import sys
from sklearn.metrics import mean_squared_error
from math import sqrt
from src.train import build_model
from src.predict import predict_ratings

current_path = os.getcwd()
module_path = os.path.dirname(os.getcwd()) + "/src"
sys.path.insert(1, module_path)

build_model(50,100,5,0.5,25,'collab')

raw, predictions = predict_ratings(model_type = 'conv_NN')

rmse = sqrt(mean_squared_error(raw["rating"].values, predictions["rating"].values))
print(rmse)