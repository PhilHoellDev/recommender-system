import requests
import xmltodict
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
import numpy as np
import string
from nltk.tokenize.treebank import TreebankWordDetokenizer
from nltk.stem import PorterStemmer, LancasterStemmer, RegexpStemmer,SnowballStemmer, WordNetLemmatizer

from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.tag.stanford import StanfordNERTagger
from nltk.corpus import wordnet, stopwords, names
nltk.download('stopwords')
import collections, re
import scipy.sparse as sparse
from gensim.models import LdaModel, LdaMulticore
from gensim import matutils
from gensim.corpora import Dictionary


def train_lda():
    #_________________Read Raw Data________________
    raw_plots =  pd.read_csv("all_omdbapi_raw.csv")
    movies =  pd.read_csv("../data/raw/movies.csv")
    raw_plots = raw_plots[[ "0", "9", "5", "17"]]
    raw_plots = raw_plots.rename({"0": "name", "9": "plot", "5": "genres", "17": "imdbID"}, axis = 1)
    raw_plots["imdbID"] = raw_plots["imdbID"].str[3:]

    #Add existing movietags here
    raw_plots["soup"] = raw_plots["name"] +" "+raw_plots["genres"]+ " " + raw_plots["plot"] 
    raw_plots["soup"] = raw_plots["soup"].fillna("")

    stop_words = stopwords.words("english")
    name_list = names.words()
    detokenizer = TreebankWordDetokenizer()
    sb = SnowballStemmer("english")


    #Preprocess text
    raw_plots["soup"] = raw_plots["soup"].str.lower()
    print("Lower Cased all words")

    raw_plots["soup"] = raw_plots["soup"].str.replace('[{}]'.format(string.punctuation), '')
    print("Removed punctiation")

    raw_plots["soup"] = raw_plots["soup"].apply(nouns_adj)
    print("Removed verbs")

    raw_plots["soup"] = raw_plots["soup"].apply(lambda x: remove_stopswords(x))
    print("Removed stopwords")

    raw_plots["soup"] = raw_plots["soup"].apply(lambda x: remove_names(x))
    print("Removed names")


    raw_plots["stemmed"] = raw_plots["soup"].apply(lambda x: stemming(x))

    raw_plots["tokenized"] = raw_plots["stemmed"].apply(lambda x: word_tokenize(x))
    dictionary = Dictionary(raw_plots["tokenized"])
    dictionary.filter_extremes(no_below=15, no_above=0.2, keep_n=1000000) #Filters Words that occur in more than no_above %, less than no_below  descriptions and only uses the keep_n most frequent words 

    bow_corpus = [dictionary.doc2bow(doc) for doc in raw_plots["tokenized"]]
    lda = LdaMulticore(bow_corpus, num_topics=25, id2word=dictionary, passes=1, workers= 8 )
    
    
    lis = []
    for i in bow_corpus:
        document_topics = lda.get_document_topics(i,minimum_probability =0.0)
        lis.append([i[1] for i in document_topics])
        
    raw_plots["LDA weighs"] = lis






def remove_stopswords(txt_tokenized):
    txt_tokenized = word_tokenize(txt_tokenized)
    stopwords_removed = [word for word in txt_tokenized if word not in stop_words]
    return detokenizer.detokenize(stopwords_removed)

def remove_names(txt_tokenized):
    txt_tokenized = word_tokenize(txt_tokenized)
    names_removed = [word for word in txt_tokenized if word not in name_list]
    return detokenizer.detokenize(names_removed)

def save_tokenized(txt_tokenized):
    txt_tokenized = word_tokenize(txt_tokenized)
    return txt_tokenized

def stemming(txt_tokenized):
    sb = SnowballStemmer("english")

    txt_tokenized = word_tokenize(txt_tokenized)
    stemmed_text = [sb.stem(y) for y in txt_tokenized]
    return detokenizer.detokenize(stemmed_text)

def nouns_adj(text): 
    is_noun_adj = lambda pos: pos[:2] == "NN" or pos[:2] == "JJ"
    tokenized = word_tokenize(text)
    all_nouns = [word for (word, pos) in pos_tag(tokenized) if is_noun_adj]
    return " ".join(all_nouns)

def name_filter(text): 
    st = StanfordNERTagger()
    is_noun_adj = lambda pos: pos[:2] == "NN" or pos[:2] == "JJ"
    tokenized = word_tokenize(text)
    all_nouns = [word for (word, pos) in st.tag(tokenized) if is_noun_adj]
    return " ".join(all_nouns)
