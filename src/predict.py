#!/usr/bin/env python
# coding: utf-8

# In[65]:

from sklearn.metrics import mean_squared_error
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime as dt
#Imports for NN
from tensorflow.keras.models import load_model
import os

#additional import statements for SKlearn models

from math import sqrt

import sys
current_path = os.getcwd()
module_path = os.path.dirname(os.getcwd()) + "/src"
sys.path.insert(1, module_path)

from .preprocessing import *

    
import string
from keras.preprocessing.sequence import pad_sequences

#from autokeras import StructuredDataRegressor


# In[66]:



n_factors = 50
max_rating = 5
min_rating = 1




#EmbeddingLayer aus Collaborative Filtering Modell von Michael
from keras.layers import Add, Activation, Lambda
class EmbeddingLayer:
    def __init__(self, n_items, n_factors):
        self.n_items = n_items
        self.n_factors = n_factors
    
    def __call__(self, x):
        x = Embedding(self.n_items, self.n_factors, embeddings_initializer='he_normal',
                      embeddings_regularizer=l2(1e-6))(x)
        x = Reshape((self.n_factors,))(x)
        return x

def RecommenderV1(n_users, n_movies, n_factors):
    user = Input(shape=(1,))
    u = Embedding(n_users, n_factors, embeddings_initializer='he_normal',
                  embeddings_regularizer=l2(1e-6))(user)
    u = Reshape((n_factors,))(u)
    
    movie = Input(shape=(1,))
    m = Embedding(n_movies, n_factors, embeddings_initializer='he_normal',
                  embeddings_regularizer=l2(1e-6))(movie)
    m = Reshape((n_factors,))(m)
    
    x = Dot(axes=1)([u, m])
    model = Model(inputs=[user, movie], outputs=x)
    opt = Adam(lr=0.001)
    model.compile(loss='mean_squared_error', optimizer=opt)
    return model


def RecommenderV2(n_users, n_movies, n_factors, min_rating, max_rating):
    user = Input(shape=(1,))
    u = EmbeddingLayer(n_users, n_factors)(user)
    ub = EmbeddingLayer(n_users, 1)(user)
    
    movie = Input(shape=(1,))
    m = EmbeddingLayer(n_movies, n_factors)(movie)
    mb = EmbeddingLayer(n_movies, 1)(movie)
    x = Dot(axes=1)([u, m])
    x = Add()([x, ub, mb])
    x = Activation('sigmoid')(x)
    x = Lambda(lambda x: x * (max_rating - min_rating) + min_rating)(x)
    model = Model(inputs=[user, movie], outputs=x)
    opt = Adam(lr=0.001)
    model.compile(loss='mean_squared_error', optimizer=opt)
    return model



# In[67]:

def predict_ratings(model_type = "collab"): 
    

    #movie_tags = pd.read_csv("../data/raw/movie_tags.csv")
    
    from pathlib import Path
    path = Path(__file__).parent #/ "../data/modeling/test/test.csv"


    ratings = pd.read_csv(path / "../data/modeling/test/test.csv")
    ratings = ratings.sort_values(by = ["user_id", "movieID"])
    ratings_raw = ratings.copy()

    ratings = ratings.rename({"user_id": "userID"}, axis =1)



    movies =  pd.read_csv(path /"../data/preprocessed/movies.csv")

    movie_tags = pd.read_csv(path /"../data/raw/movie_tags.csv")
    tags = pd.read_csv(path /"../data/raw/tags.csv")
    genres = pd.read_csv(path /"../data/preprocessed/genres.csv")
    movies = movies.rename({"id": "movieID"}, axis =1)
    #genres = genres.set_index("movieID")
    movies = movies.set_index("movieID")  #Set Index to movieID
    tags = tags.rename({"id": "tagID"}, axis =1)
    directors = pd.read_csv(path /"../data/preprocessed/directors.csv")
    actors = pd.read_csv(path /"../data/preprocessed/actors.csv")
    countries = pd.read_csv(path /"../data/preprocessed/countries.csv")

    #Read and restructure LDA Tags
    lda_data = pd.read_csv(path /"../data/preprocessed/LDA_results_1500passes_20words.csv")
    lda_data.LDA_tags = lda_data.LDA_tags.str.replace("[", "").str.replace("]", "").str.replace(",", "")
    lda_data.LDA_tags = lda_data.LDA_tags.apply(np.fromstring, dtype = float, sep = " ")

    #Read LDA Data and merge it into the movies_df
    #lda_data =  pd.read_csv("../data/preprocessed/LDA_results.csv")

    duplicates =  pd.read_csv(path /"../data/raw/df_duplicates_overview.csv", sep = ',')
    try: 
        duplicates = duplicates.drop(["Unnamed: 0"],axis = 1)
    except: 
        print("Could not find Unnamed columns")
    #movies = movies[movies.index.isin(ratings.movieID)]   #Filter all movies that have not been rated (128)
    #ratings = ratings.iloc[1:150000]
    SPARSE= True

    ratings["rating"] = -1
    ratings = ratings.sort_values(by = ["userID", "movieID"])

    # In[70]:



    duplicates.index = duplicates["1. Vorkommen"]
    dictionary=  {}
    for i in duplicates.iloc[:,1:].columns:
        additional_dict = duplicates[~duplicates[i].isna()][i].to_dict()
        additional_dict = {y:x for x,y in additional_dict.items()}
        dictionary= {**dictionary, **additional_dict}

    ratings["movieID"]= ratings["movieID"].replace(dictionary)


    # In[71]:


    movies = add_actors(movies, actors)

    movies = movies.join(lda_data.set_index("movieID")[["LDA_tags"]])
    movies = movies.join(directors.set_index("movieID")[["directorID"]])
    movies = movies.join(countries.set_index("movieID")[["country"]])
    movies["directorID"] = movies["directorID"].fillna("")
    movies["country"] = movies["country"].fillna("")


    movies_ratings_merged = ratings.merge(movies[["year", "amount_of_imdbUserRatings","imdb_user_rating", "directorID", "country", "length", "LDA_tags", "actorstring", "plot"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    
    #Load the pickled Encoder object fitted by train.py
    encoder = Encoder()
    encoder = encoder.load()


    MAX_ACTOR_LENGTH = 10  #Defines Number of relevant actors in ranking
    tokenized_actors= encoder.actor_tokenizer.texts_to_sequences(movies["actorstring"].values)
    movies["tokenized_actors"] = [i for i in pad_sequences(tokenized_actors, maxlen = MAX_ACTOR_LENGTH, padding = "post", truncating = "post")]

    actor_index = encoder.actor_tokenizer.word_index


    MAX_SEQUENCE_LENGTH = 200  #Defines Cut off 
    tokenized_plots = encoder.plot_tokenizer.texts_to_sequences(movies["plot"].values)
    movies["tokenized_plots"]  = [i for i in pad_sequences(tokenized_plots, maxlen = MAX_SEQUENCE_LENGTH, padding = "post", truncating = "post")]

    word_index = encoder.plot_tokenizer.word_index


    X_features_predict, _ = preprocessing_pipeline(ratings, movies, encoder, standardscale = True,genres = genres, years = True, encode_user = "not",encode_movieID = "not", 
                               length = True, amount_of_imdbUserRatings = True, imdb_user_rating = True)


    movies_ratings_merged_predict = ratings.merge(movies[["year", "amount_of_imdbUserRatings","imdb_user_rating", "length", "LDA_tags", "actorstring", "plot"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    movies_ratings_merged_predict = movies_ratings_merged_predict.merge(directors[["directorID", "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    movies_ratings_merged_predict = movies_ratings_merged_predict.merge(countries[["country", "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    mm = movies.reset_index()
    movies_ratings_merged_predict = movies_ratings_merged_predict.merge(mm[["tokenized_plots","tokenized_actors",  "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    movies_ratings_merged_predict = movies_ratings_merged_predict.fillna("")



    #LabelEncode Users
    X_user_predict = encoder.user_enc.transform(movies_ratings_merged_predict.sort_values(by = ["userID", "movieID"])['userID'].values)

    X_movieID_predict = encoder.item_enc.transform(movies_ratings_merged_predict.sort_values(by = ["userID", "movieID"])['movieID'].values)

    X_director_predict = encoder.director_enc.transform(movies_ratings_merged_predict.sort_values(by = ["userID", "movieID"])['directorID'].values.astype(str))

    X_country_predict = encoder.country_enc.transform(movies_ratings_merged_predict.sort_values(by = ["userID", "movieID"])['country'].values.astype(str))


    X_plots_predict = np.vstack(movies_ratings_merged_predict["tokenized_plots"].values)

    X_actors_predict = np.vstack(movies_ratings_merged_predict["tokenized_actors"].values)


    X_pred_array = []
    X_pred_array.append(X_user_predict)
    X_pred_array.append(X_movieID_predict)
    X_pred_array.append(X_features_predict)
    X_pred_array.append(X_plots_predict)
    X_pred_array.append(X_director_predict )
    X_pred_array.append(X_country_predict)
    X_pred_array.append(X_actors_predict)

    print(X_pred_array)
    if model_type == "collab":
        X_pred_array = [X_pred_array[0], X_pred_array[1]]   
    elif model_type == "conv": 
        X_pred_array = [X_pred_array[0], X_pred_array[2], X_pred_array[3],X_pred_array[4], X_pred_array[5],X_pred_array[6]]
    elif model_type == "old_NN":
        X_pred_array = [X_train_array[0],X_train_array[2]]

    # In[84]:
    from pathlib import Path
    path = Path(__file__).parent #/ "../data/modeling/test/test.csv"

            

    modelCB = load_model(path/ '../models/model.h5')


    ratings["rating"] = modelCB.predict(X_pred_array)

    test_data_with_rating = ratings.astype(np.float64)

    
    
    return ratings_raw, test_data_with_rating