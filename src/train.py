### Import libraries###
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime as dt
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf
import os

from scipy import sparse
from sklearn.metrics import mean_squared_error, mean_absolute_error
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

from datetime import datetime as dt
from math import sqrt

from sklearn.ensemble import RandomForestRegressor

import sys
current_path = os.getcwd()
module_path = os.path.abspath(os.path.join(os.pardir,'src'))
sys.path.insert(1, module_path)
from .preprocessing import *

import string
from sklearn.preprocessing import LabelEncoder
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder

from keras.models import Model
from keras.layers import Input, Reshape, Dot, Conv1D, MaxPooling1D, Flatten
from keras.layers.embeddings import Embedding
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.layers import Add, Activation, Lambda
from tensorflow.keras.layers import Dense, Activation, LSTM, concatenate, AveragePooling1D

from tensorflow.keras.callbacks import ModelCheckpoint
from tensorflow.keras.callbacks import EarlyStopping

### Global Variables ###
n_factors = 50
MAX_RATING = 5
MIN_RATING = 0.5
SPARSE= True
MAX_ACTOR_LENGTH = 10 
MAX_SEQUENCE_LENGTH = 150 

### Load data ###
movies =  pd.read_csv("data/modeling/train/movies.csv")

###TODO: Change path of ratings!
ratings = pd.read_csv("data/modeling/train/ratings_train.csv")
#ratings = pd.read_csv(/data/preprocessed/ratings1.csv")
lda_data = pd.read_csv("data/modeling/train/LDA_results_1500passes_20words.csv")

movie_tags = pd.read_csv("data/modeling/train/movie_tags.csv")
tags = pd.read_csv("data/modeling/train/tags.csv")

genres = pd.read_csv("data/modeling/train/genres.csv")
directors = pd.read_csv("data/modeling/train/directors.csv")
actors = pd.read_csv("data/modeling/train/actors.csv")
countries = pd.read_csv("data/modeling/train/countries.csv")

class EmbeddingLayer:
    def __init__(self, n_items, n_factors):
        self.n_items = n_items
        self.n_factors = n_factors
    
    def __call__(self, x):
        x = Embedding(self.n_items, self.n_factors, embeddings_initializer='he_normal',
                      embeddings_regularizer=l2(1e-6))(x)
        x = Reshape((self.n_factors,))(x)
        return x

def RecommenderV2(n_users, n_movies, n_factors, min_rating, max_rating):
    user = Input(shape=(1,))
    u = EmbeddingLayer(n_users, n_factors)(user)
    ub = EmbeddingLayer(n_users, 1)(user)
    
    movie = Input(shape=(1,))
    m = EmbeddingLayer(n_movies, n_factors)(movie)
    mb = EmbeddingLayer(n_movies, 1)(movie)
    x = Dot(axes=1)([u, m])
    x = Add()([x, ub, mb])
    x = Activation('sigmoid')(x)
    x = Lambda(lambda x: x * (max_rating - min_rating) + min_rating)(x)
    model = Model(inputs=[user, movie], outputs=x)
    opt = Adam(lr=0.001)
    model.compile(loss='mean_squared_error', optimizer=opt)
    return model
    
def prepare_inputs(movies,ratings,lda_data, tags):

    tags = tags.rename({"id": "tagID"}, axis =1)

    movies = movies.rename({"id": "movieID"}, axis =1)
    movies = movies.set_index("movieID") 
    movies = add_actors(movies, actors)

    #movies = movies.join(lda_data.set_index("movieID")[["LDA_tags"]])

    ratings = ratings.rename({"user_id": "userID"}, axis =1)
    
    lda_data.LDA_tags = lda_data.LDA_tags.str.replace("[", "").str.replace("]", "").str.replace(",", "")
    lda_data.LDA_tags = lda_data.LDA_tags.apply(np.fromstring, dtype = float, sep = " ")

    movies = movies.join(lda_data.set_index("movieID")[["LDA_tags"]])
    movies = movies.join(directors.set_index("movieID")[["directorID"]])
    movies = movies.join(countries.set_index("movieID")[["country"]])
    movies["directorID"] = movies["directorID"].fillna("")
    movies["country"] = movies["country"].fillna("")

    movies_ratings_merged = ratings.merge(movies[["year", "amount_of_imdbUserRatings","imdb_user_rating", "directorID", "country", "length", "LDA_tags", "actorstring", "plot"]], on = "movieID").sort_values(by = ["userID"])
    

    # Split ratings
    split = np.random.rand(len(ratings)) < 0.8
    ratings_train = ratings[split].sort_values(by = ["userID", "movieID"])
    ratings_test = ratings[~split].sort_values(by = ["userID", "movieID"])

    # Add syntetic ratings to avoi the cold start problem
    ratings_train_ext = ratings_train
    #ratings_train_ext = add_syn_ratings(ratings_train, 2, 15)
    #ratings_train_ext = ratings_train_ext.sort_values(by = ["userID", "movieID"])

    return movies, ratings, tags, movies_ratings_merged, ratings_train, ratings_test, ratings_train_ext



def tokenize_data(movies, encoder):
    tokenized_actors= encoder.actor_tokenizer.texts_to_sequences(movies["actorstring"].values)
    movies["tokenized_actors"] = [i for i in pad_sequences(tokenized_actors, maxlen = MAX_ACTOR_LENGTH, padding = "post", truncating = "post")]

    actor_index = encoder.actor_tokenizer.word_index

    tokenized_plots = encoder.plot_tokenizer.texts_to_sequences(movies["plot"].values)
    movies["tokenized_plots"]  = [i for i in pad_sequences(tokenized_plots, maxlen = MAX_SEQUENCE_LENGTH, padding = "post", truncating = "post")]

    word_index = encoder.plot_tokenizer.word_index

    return actor_index, word_index



def prepare_conv_nn(movies, encoder, ratings_test, ratings_train_ext,X_features_test, X_features_train):
    mm = movies.reset_index()
    movies_ratings_merged_train = ratings_train_ext.merge(movies[["year", "amount_of_imdbUserRatings","imdb_user_rating", "length", "LDA_tags", "actorstring", "plot"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    movies_ratings_merged_test = ratings_test.merge(movies[["year", "amount_of_imdbUserRatings","imdb_user_rating", "length", "LDA_tags", "actorstring", "plot"]], on = "movieID").sort_values(by = ["userID", "movieID"])

    
    movies_ratings_merged_train = movies_ratings_merged_train.merge(mm[["tokenized_plots","tokenized_actors",  "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])


    movies_ratings_merged_train = movies_ratings_merged_train.merge(directors[["directorID", "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    movies_ratings_merged_train = movies_ratings_merged_train.merge(countries[["country", "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    mm = movies.reset_index()



    movies_ratings_merged_test = movies_ratings_merged_test.merge(directors[["directorID", "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    movies_ratings_merged_test = movies_ratings_merged_test.merge(countries[["country", "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    mm = movies.reset_index()
    movies_ratings_merged_test = movies_ratings_merged_test.merge(mm[["tokenized_plots","tokenized_actors",  "movieID"]], on = "movieID").sort_values(by = ["userID", "movieID"])

    X_user_train = encoder.user_enc.transform(movies_ratings_merged_train['userID'].fillna("").values)
    X_user_test = encoder.user_enc.transform(movies_ratings_merged_test['userID'].fillna("").values)
    n_users = encoder.user_enc.classes_.shape[0] 

    X_movieID_train = encoder.item_enc.transform(movies_ratings_merged_train['movieID'].fillna("").values)
    X_movieID_test = encoder.item_enc.transform(movies_ratings_merged_test['movieID'].fillna("").values)
    n_movies = encoder.item_enc.classes_.shape[0]

    X_director_train = encoder.director_enc.transform(movies_ratings_merged_train['directorID'].fillna("").values.astype(str))
    X_director_test = encoder.director_enc.transform(movies_ratings_merged_test['directorID'].fillna("").values.astype(str))
    n_directors = encoder.director_enc.classes_.shape[0]

    X_country_train = encoder.country_enc.transform(movies_ratings_merged_train['country'].fillna("").values.astype(str))
    X_country_test = encoder.country_enc.transform(movies_ratings_merged_test['country'].fillna("").values.astype(str))
    n_country = encoder.country_enc.classes_.shape[0]

    X_plots_train = np.vstack(movies_ratings_merged_train["tokenized_plots"].fillna("").values)
    X_plots_test = np.vstack(movies_ratings_merged_test["tokenized_plots"].fillna("").values)

    X_actors_train = np.vstack(movies_ratings_merged_train["tokenized_actors"].fillna("").values)
    X_actors_test = np.vstack(movies_ratings_merged_test["tokenized_actors"].fillna("").values)

    X_train_array = []
    X_test_array = []

    X_train_array.append(X_user_train)
    X_test_array.append(X_user_test)

    X_train_array.append(X_movieID_train)
    X_test_array.append(X_movieID_test)

    X_train_array.append(X_features_train)
    X_test_array.append(X_features_test)

    X_train_array.append(X_plots_train)
    X_test_array.append(X_plots_test)

    X_train_array.append(X_director_train )
    X_test_array.append(X_director_test)

    X_train_array.append(X_country_train)
    X_test_array.append(X_country_test)

    X_train_array.append(X_actors_train)
    X_test_array.append(X_actors_test)

    return X_train_array, X_test_array, n_users, n_movies, n_directors, n_country

def build_model(n_factors_movie = 50, n_factors_user = 100, MAX_RATING = 5, MIN_RATING = 0.5, EPOCHS = 25, model_type = None):
    global movies,ratings, lda_data, tags, genres
    movies, ratings, tags, movies_ratings_merged, ratings_train, ratings_test, ratings_train_ext = prepare_inputs(movies,ratings,lda_data,tags)
    
    encoder = encode_data(movies,ratings, movies_ratings_merged)

    actor_index, word_index = tokenize_data(movies, encoder)

    #movies = pd.read_csv("../data/modeling/train/movies.csv")
    #movies = movies.rename({"id": "movieID"}, axis =1)
    #movies = movies.set_index("movieID")

    X_features_train, y_train, X_features_test, y_test = preprocess_inputs(ratings, movies, lda_data, ratings_train_ext, encoder, ratings_test, genres)
    
    ### TODO: PRÜFEN WIE DAS DF HIER ERZEUGT WERDEN SOLL ###
    #movies_ratings_merged_train  = movies_ratings_merged 
    #movies_ratings_merged_train

    X_train_array, X_test_array, n_users, n_movies, n_directors, n_country = prepare_conv_nn(movies,  encoder, ratings_test, ratings_train_ext,X_features_test, X_features_train)

    x_content_train = X_train_array[2]
    
    print(X_train_array)
    
    if model_type=='old_NN':

        user = Input(shape=(1,))

        #u = Embedding(n_users, n_factors_user, embeddings_initializer='he_normal', embeddings_regularizer=l2(1e-6))(user)
        #u = Reshape((n_factors_user,))(user)
        u = EmbeddingLayer(n_users, n_factors_user)(user)

        feature = Input(shape=(x_content_train.shape[1],))
        feat = Dense(200, activation="relu")(feature)

        combined = concatenate([u,feat])

        z = Dense(200, activation="relu")(combined)
        z = Dense(200, activation="relu")(z)

        z = Dense(1, activation="relu")(z)
        z = Activation('relu')(z)
        z = Lambda(lambda z: z * (MAX_RATING - MIN_RATING) + MIN_RATING)(z)

        model = Model(inputs=[user, feature], outputs=z)

        opt = Adam(lr=0.001)
        model.compile(loss='mean_squared_error', optimizer=opt)

        model.fit(x=[X_train_array[0],X_train_array[2]], y=y_train, batch_size=64, epochs=EPOCHS,
                        verbose=1, validation_data=([X_test_array[0],X_test_array[2]], y_test))

        
    if model_type=='collab':
        n_factors = 50
        opt = Adam(lr=0.001)

        early_stopping = EarlyStopping( monitor='val_loss', patience=2, verbose=1,restore_best_weights=True)

        model = RecommenderV2(n_users, n_movies, n_factors, max_rating = 5, min_rating = 0.5)
        model.summary()
        model.compile(loss='mean_squared_error', optimizer=opt)
        history = model.fit(x = [X_train_array[0],X_train_array[1]], y = y_train, batch_size=32, epochs=30,
                            verbose=1, callbacks = [early_stopping], validation_data=([X_test_array[0],X_test_array[1]], y_test))
    max_rating = 5 
    min_rating = 0.5
    
    if model_type=='conv_NN':
        
        EMBEDDING_DIM = 300
        embedding_matrix = np.zeros((len(word_index) + 1, EMBEDDING_DIM))
        embedding_matrix.shape
        len(word_index)


        GLOVE_DIR = "data/modeling/train/glove.6B.300d.txt"
        embeddings_index = {}
        f = open(GLOVE_DIR, encoding="utf8")
        for line in f:
            values = line.split()
            word = values[0]
            coefs = np.asarray(values[1:], dtype='float32')
            embeddings_index[word] = coefs
        f.close()
        
        EMBEDDING_DIM = 300
        embedding_matrix = np.zeros((len(word_index) + 1, EMBEDDING_DIM))
        for word, i in word_index.items():
            embedding_vector = embeddings_index.get(word)
           # print(i)
            if embedding_vector is not None:
                # words not found in embedding index will be all-zeros.
                embedding_matrix[i] = embedding_vector
        
        embedding_layer = Embedding(len(word_index) + 1,
                            EMBEDDING_DIM,
                            weights=[embedding_matrix],
                            input_length=MAX_SEQUENCE_LENGTH,
                            trainable=False)


        

        early_stopping = EarlyStopping( monitor='val_loss', patience=8, verbose=1,restore_best_weights=True)

        n_factors = 125
        n_factors_directors = 20
        min_ratings = 0.5
        max_ratings = 5

        user_input = Input(shape=(1,))
        u = EmbeddingLayer(n_users, n_factors)(user_input)

        movie_input = Input(shape=(1,))
        m = EmbeddingLayer(n_movies, n_factors)(movie_input)

        directors_input = Input(shape=(1,))
        d = EmbeddingLayer(n_directors, n_factors_directors)(directors_input)

        country_input = Input(shape=(1,))
        c = EmbeddingLayer(n_country, 5)(country_input)


        actor_input = Input(shape=(10,))
        embedded_actors = Embedding(len(actor_index) + 1, 10, input_length = MAX_ACTOR_LENGTH, trainable=True)(actor_input) #Next step: Increase Actor Embedding
        act = Conv1D(128, 5, activation='relu')(embedded_actors)   
        act = MaxPooling1D(5)(act)
        act = Conv1D(128, 5, activation='relu', data_format='channels_first')(act)
        act = MaxPooling1D(5)(act) 
        act = Flatten()(act)
        act =Dense(20, activation='relu')(act)

        #act = Dense(100, activation='relu')(act)

        sequence_input = Input(shape=(MAX_SEQUENCE_LENGTH,), dtype='int32')

        #Topic Modelling with 
        embedded_sequences = embedding_layer(sequence_input)
        x = Conv1D(128, 5, activation='relu')(embedded_sequences)   
        x = MaxPooling1D(5)(x)
        x = Conv1D(128, 5, activation='relu')(x)
        x = MaxPooling1D(5)(x)
        x = Conv1D(128,5, activation='relu', data_format='channels_first')(x)
        x = MaxPooling1D(35)(x)  # global max pooling
        x = Flatten()(x)
        x = Dense(100, activation='relu')(x)

        feature_input = Input(shape=(X_train_array[2].shape[1],))
        feature = Dense(44)(feature_input)

        #final = concatenate([ feature,u])

        final = concatenate([ feature, x, d, c, u, act])
        #final = Dropout(0.1)(final)

        final = Dense(300, activation = "relu")(final)
        final = Dense(600, activation = "relu")(final)
        #final = Dropout(0.1)(final)

        final = Dense(300, activation = "relu")(final)
        final = Dense(50,activation = "relu")(final)


        final = Dense(1, activation = "relu")(final)
        final = Lambda(lambda x: x * (max_rating - min_rating) + min_rating)(final)


        #model = Model(inputs=[user_input, movie_input, feature_input, sequence_input, directors_input, cf_input], outputs=final)
        model = Model(inputs=[user_input,  feature_input, sequence_input, directors_input, country_input, actor_input], outputs=final)

        opt = Adam(lr=0.001)
        model.compile(loss='mean_squared_error', optimizer=opt)
        
        history = model.fit(x = [X_train_array[0], X_train_array[2], X_train_array[3],X_train_array[4], X_train_array[5],X_train_array[6]] , y = y_train,callbacks = [early_stopping], batch_size=200, epochs=1,
                            verbose=1, validation_data=( [X_test_array[0], X_test_array[2], X_test_array[3],X_test_array[4], X_test_array[5], X_test_array[6]], y_test))


    
    
    
    model.save("models/model.h5")  
    print("Saved model")

if __name__=="__main__":
    build_model(50,100,5,0.5,25,'collab')
