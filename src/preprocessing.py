import pandas as pd
import numpy as np
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import LabelEncoder, StandardScaler

from datetime import datetime as dt
from scipy import sparse
from category_encoders.basen import BaseNEncoder
#from predict import collaborative_filtering_NN

import requests
import xmltodict
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import nltk
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
import numpy as np
import string
from nltk.tokenize.treebank import TreebankWordDetokenizer
from nltk.stem import PorterStemmer, LancasterStemmer, RegexpStemmer,SnowballStemmer, WordNetLemmatizer
from nltk.stem import PorterStemmer
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.tag.stanford import StanfordNERTagger
from nltk.corpus import wordnet, stopwords, names
nltk.download('stopwords')
import collections, re
import scipy.sparse as sparse
from gensim.models import LdaModel, LdaMulticore
from gensim import matutils
from gensim.corpora import Dictionary
from collections import OrderedDict
from keras.preprocessing.text import Tokenizer
import pickle

from collections import Counter
def counter_word(text):
    count = Counter()
    for i in text: 
        for word in i.split():
            count[word]+=1
    return count

class Encoder():
    user_enc = LabelEncoder()
    item_enc = LabelEncoder()
    director_enc = LabelEncoder()
    country_enc = LabelEncoder()
    
    lda_scaler = StandardScaler()
    year_scaler = StandardScaler()
    length_scaler = StandardScaler()
    amount_imdb_ratings_scaler = StandardScaler()
    imdb_ratings_scaler = StandardScaler()
    
    actor_tokenizer = Tokenizer()
    plot_tokenizer = Tokenizer()
    
    def save(self): 
        with open('src/pickle/user_enc.pkl', 'wb') as output:
            pickle.dump(self.user_enc, output)
        with open('src/pickle/item_enc.pkl', 'wb') as output:
            pickle.dump(self.item_enc, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/director_enc.pkl', 'wb') as output:
            pickle.dump(self.director_enc, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/country_enc.pkl', 'wb') as output:
            pickle.dump(self.country_enc, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/lda_scaler.pkl', 'wb') as output:
            pickle.dump(self.lda_scaler, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/year_scaler.pkl', 'wb') as output:
            pickle.dump(self.year_scaler, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/length_scaler.pkl', 'wb') as output:
            pickle.dump(self.length_scaler, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/amount_imdb_ratings_scaler.pkl', 'wb') as output:
            pickle.dump(self.amount_imdb_ratings_scaler, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/imdb_ratings_scaler.pkl', 'wb') as output:
            pickle.dump(self.imdb_ratings_scaler, output, protocol=pickle.HIGHEST_PROTOCOL)
            
        with open('src/pickle/actor_tokenizer.pkl', 'wb') as output:
            pickle.dump(self.actor_tokenizer, output, protocol=pickle.HIGHEST_PROTOCOL)
        with open('src/pickle/plot_tokenizer.pkl', 'wb') as output:
            pickle.dump(self.plot_tokenizer, output, protocol=pickle.HIGHEST_PROTOCOL)
                
            
    def load(self): 
        from pathlib import Path
        path = Path(__file__).parent #/ "../data/modeling/test/test.csv"

            
        with open(path/'../src/pickle/user_enc.pkl', 'rb') as input:
            self.user_enc = pickle.load(input)
        with open(path/'../src/pickle/item_enc.pkl', 'rb') as input:
            self.item_enc = pickle.load(input)
        with open(path/'../src/pickle/director_enc.pkl', 'rb') as input:
            self.director_enc = pickle.load(input)
        with open(path/'../src/pickle/country_enc.pkl', 'rb') as input:
            self.country_enc = pickle.load(input)
            
        with open(path/'../src/pickle/lda_scaler.pkl', 'rb') as input:
            self.lda_scaler = pickle.load(input)
        with open(path/'../src/pickle/year_scaler.pkl', 'rb') as input:
            self.year_scaler = pickle.load(input)
        with open(path/'../src/pickle/length_scaler.pkl', 'rb') as input:
            self.length_scaler = pickle.load(input)
        with open(path/'../src/pickle/amount_imdb_ratings_scaler.pkl', 'rb') as input:
            self.amount_imdb_ratings_scaler = pickle.load(input)
        with open(path/'../src/pickle/imdb_ratings_scaler.pkl', 'rb') as input:
            self.imdb_ratings_scaler = pickle.load(input)
            
        with open(path/'../src/pickle/actor_tokenizer.pkl', 'rb') as input:
            self.actor_tokenizer = pickle.load(input)
        with open(path/'../src/pickle/plot_tokenizer.pkl', 'rb') as input:
            self.plot_tokenizer = pickle.load(input)
                
        return self
    
    def fit_LabelEncoder(self, ratings, movies):
        self.user_enc.fit(ratings['userID'].values)
        self.item_enc.fit(movies.index.values)
        self.director_enc.fit(movies["directorID"].values)
        self.country_enc.fit(movies["country"].values)
        
    def fit_StandardScaler(self, movies_ratings_merged):
        self.year_scaler.fit(movies_ratings_merged["year"].values.reshape(-1,1))
        self.amount_imdb_ratings_scaler.fit(movies_ratings_merged["amount_of_imdbUserRatings"].values.reshape(-1,1))
        self.imdb_ratings_scaler.fit(movies_ratings_merged["imdb_user_rating"].values.reshape(-1,1))
        self.length_scaler.fit(movies_ratings_merged["length"].values.reshape(-1,1))
        X_lda = np.concatenate(movies_ratings_merged["LDA_tags"].values).reshape(-1,len(movies_ratings_merged["LDA_tags"][0])).astype("float32") 
        self.lda_scaler.fit(X_lda)
        
    def fit_act_tokenizer(self,movies):
        actor_list = movies["actorstring"].values#.tolist()
        num_actors = len(counter_word(actor_list))
        
        self.actor_tokenizer = Tokenizer(num_words = num_actors,filters = "", lower = False, split=' ')
        self.actor_tokenizer.fit_on_texts(actor_list)
        
    def fit_plot_tokenizer(self,movies):
        plot_list = movies["plot"].values#.tolist()
        num_words = len(counter_word(plot_list))
        
        self.plot_tokenizer = Tokenizer(num_words = num_words,filters = "", lower = False, split=' ')
        self.plot_tokenizer.fit_on_texts(plot_list)
        
        
def add_actors(movies, actors):
    actors[["actorID", "ranking"]] = actors[["actorID", "ranking"]].fillna(" ")

    actors.ranking = actors.ranking.str.replace("[", "").str.replace("]", "").str.replace(",", "").str.replace("'", "")
    actors.actorID = actors.actorID.str.replace("[", "").str.replace("]", "").str.replace(",", "").str.replace("'", "")

    actors.ranking = actors.ranking.apply(np.fromstring, dtype = float, sep = " ")
    actors.actorID = actors.actorID.apply(str.split, sep = " ")

    actor_list = [np.array(list(OrderedDict(sorted(zip(i[1], i[0]))).values())) for i in actors[["actorID", "ranking"]].values]
    actors["actorstring"] = [' '.join(map(str, l)) for l in actor_list]

    movies = movies.join(actors["actorstring"])
    movies["actorstring"] = movies["actorstring"].fillna("")
    return movies

    
def preprocessing_pipeline(ratings, movies, encoder, movie_tags = None,tags = None,  genres = None, directors = None, 
                           lda_data = None, SPARSE = False, encode_user = "label",encode_movieID = "label", standardscale = True,cf_res = False, years = False, 
                           length = False, amount_of_imdbUserRatings = False, imdb_user_rating = False):
    """
    Preprocesses all DataFrames provided, returns a Numpy 
    """
    ratings = ratings.sort_values(by=["userID", "movieID"])


    if (isinstance(movie_tags, pd.DataFrame)) and (isinstance(tags, pd.DataFrame)):
        movies = movies.join(encode_tags(movies, movie_tags, tags, SPARSE = SPARSE))
    if (isinstance(genres, pd.DataFrame)):
        movies = movies.join(encode_genres(movies, genres, SPARSE = SPARSE))
    if (isinstance(directors, pd.DataFrame)): 
        movies = movies.join(encode_directors(movies, directors, SPARSE = SPARSE))
    if (isinstance(lda_data, pd.DataFrame)): 
        lda_data = lda_data.set_index("movieID")[["LDA_tags"]]

        #movies = movies.join(lda_data)
    #OneHotEncodes Users and Creates Sklearn/Tensorflow Input
    #print(movies["LDA_tags"])
    
    X,y= create_input(ratings, movies, encoder, SPARSE = SPARSE,  encode_user = encode_user, encode_movieID = encode_movieID, standardscale = standardscale, years = years, length = length, 
                      amount_of_imdbUserRatings = amount_of_imdbUserRatings,imdb_user_rating = imdb_user_rating,  cf_res = cf_res)
    return X,y 


def create_plot_input(ratings, movies,plot = False, max_length = 100):

    start = dt.now()
    movies_ratings_merged = ratings
    
    if "stemmed" not in movies.columns: 
        movies = text_mining_preprocess(movies)
    movies_ratings_merged = movies_ratings_merged.merge(movies[["stemmed"]], on = "movieID").sort_values(by = ["userID", "movieID"])
    plot = movies_ratings_merged["stemmed"].values#.tolist()
           
    print("Concatenated Stemmed plots, total time elapsed: ", (dt.now()-start))

    
    
    from collections import Counter
    def counter_word(text):
        count = Counter()
        for i in text: 
            for word in i.split():
                count[word]+=1
        return count
    num_words = len(counter_word(plot))
    from keras.preprocessing.text import Tokenizer
    tokenizer = Tokenizer(num_words = num_words)

    tokenizer.fit_on_texts(plot)
    tokenized_plots = tokenizer.texts_to_sequences(plot)

    
    from keras.preprocessing.sequence import pad_sequences
    padded_plots = pad_sequences(tokenized_plots, maxlen = max_length, padding = "post", truncating = "post")

    return padded_plots


def text_mining_preprocess(raw_plots):
    raw_plots["soup"] = raw_plots["plot"] #+ raw_plots["name"] +" " + raw_plots["genres"]+ " " +
    raw_plots["soup"] = raw_plots["soup"].fillna("")
    stop_words = stopwords.words("english")
    name_list = names.words()
    detokenizer = TreebankWordDetokenizer()
    sb = SnowballStemmer("english")


    def remove_stopswords(txt_tokenized):
        txt_tokenized = word_tokenize(txt_tokenized)
        stopwords_removed = [word for word in txt_tokenized if word not in stop_words]
        return detokenizer.detokenize(stopwords_removed)

    def remove_names(txt_tokenized):
        txt_tokenized = word_tokenize(txt_tokenized)
        names_removed = [word for word in txt_tokenized if word not in name_list]
        return detokenizer.detokenize(names_removed)

    def save_tokenized(txt_tokenized):
        txt_tokenized = word_tokenize(txt_tokenized)
        return txt_tokenized

    def stemming(txt_tokenized):
        sb = SnowballStemmer("english")

        txt_tokenized = word_tokenize(txt_tokenized)
        stemmed_text = [sb.stem(y) for y in txt_tokenized]
        return detokenizer.detokenize(stemmed_text)

    def nouns_adj(text): 

        is_noun_adj = lambda pos: pos[:2] == "NN" or pos[:2] == "JJ"
        tokenized = word_tokenize(text)
        all_nouns = [word for (word, pos) in pos_tag(tokenized) if is_noun_adj]
        return " ".join(all_nouns)

    def name_filter(text): 
        st = StanfordNERTagger()
        is_noun_adj = lambda pos: pos[:2] == "NN" or pos[:2] == "JJ"
        tokenized = word_tokenize(text)
        all_nouns = [word for (word, pos) in st.tag(tokenized) if is_noun_adj]
        return " ".join(all_nouns)


    #print(raw_plots["plot"])


    raw_plots["soup"] = raw_plots["soup"].str.lower()
    print("Lower Cased all words")

    raw_plots["soup"] = raw_plots["soup"].str.replace('[{}]'.format(string.punctuation), '')
    print("Removed punctiation")

    raw_plots["soup"] = raw_plots["soup"].apply(nouns_adj)
    print("Removed verbs")

    raw_plots["soup"] = raw_plots["soup"].apply(lambda x: remove_stopswords(x))
    print("Removed stopwords")

    raw_plots["soup"] = raw_plots["soup"].apply(lambda x: remove_names(x))
    print("Removed names")


    raw_plots["stemmed"] = raw_plots["soup"].apply(lambda x: stemming(x))

    return raw_plots




def preprocessing_clustering(ratings, movies, movie_tags = None,tags = None,  genres = None, directors = None, 
                           lda_data = None, SPARSE = False, encode_user = "label",encode_movieID = "label", standardscale = True,cf_res = False, years = False):
    """
    Preprocesses all DataFrames provided, returns a Numpy 
    """
    ratings = ratings.sort_values(by=["userID", "movieID"])


    if (isinstance(movie_tags, pd.DataFrame)) and (isinstance(tags, pd.DataFrame)):
        movies = movies.join(encode_tags(movies, movie_tags, tags, SPARSE = SPARSE))
    if (isinstance(genres, pd.DataFrame)):
        movies = movies.join(encode_genres(movies, genres, SPARSE = SPARSE))
    if (isinstance(directors, pd.DataFrame)): 
        movies = movies.join(encode_directors(movies, directors, SPARSE = SPARSE))
    if (isinstance(lda_data, pd.DataFrame)): 
        lda_data = lda_data.set_index("movieID")[["LDA_tags"]]

        movies = movies.join(lda_data)
    #OneHotEncodes Users and Creates Sklearn/Tensorflow Input
    #print(movies["LDA_tags"])
    
    return movies


def encode_tags(movies, movie_tags, tags, SPARSE = False):
    movie_tags = movie_tags[movie_tags['tagID'].isin(movie_tags['tagID'].value_counts()[movie_tags['tagID'].value_counts()>30].index)]
    sum_weight = movie_tags.groupby("movieID")["tagWeight"].sum()
    sum_weight = pd.DataFrame(sum_weight)
    movie_tags.tagWeight = movie_tags.tagWeight / movie_tags.movieID.map(sum_weight["tagWeight"].to_dict()) 

    movie_tags = pd.concat([movie_tags,pd.DataFrame({"movieID": movies.index, "tagID": -1, "tagWeight": 0})]) #Add -1 tag for all tags movieID's without a tag
    #movie_tags.head()
    tags = tags[tags['tagID'].isin(movie_tags['tagID'].values)] #Delete all Tags that are not in movie_tags
    

    tags["tagWeight"] = 0
    
    tags = tags.append({"tagID":-1, "value": "", "tagWeight":0}, ignore_index = True)
    tag_dict = tags.set_index("tagID")[["tagWeight"]].to_dict()

    dv_tags = DictVectorizer(sparse = SPARSE, dtype = float) #Depending on the expected input, sparse may be activated to save memory and computing time
    dv_tags.fit_transform(tag_dict["tagWeight"]) #Print number of unique items
    tag_series = movie_tags.set_index("tagID").groupby("movieID")[["tagWeight"]].apply(pd.DataFrame.to_dict, "dict").apply(lambda row: row['tagWeight']).apply(dv_tags.transform).apply(np.ravel)
    tag_series.name = "tags"
    
    return tag_series


def encode_genres(movies, genres, SPARSE = False):
        if SPARSE == False:
            genre_merged = movies.merge(genres, on = "movieID", how = "left")
            g_dummies = genre_merged[['movieID']].join(pd.get_dummies(genre_merged['genre'])).groupby('movieID').max()
            g_dummies['genre'] = g_dummies.values.tolist()
            g_dummies = g_dummies[['genre']]
            return g_dummies

        else: 
            genres["genreWeight"] = 1
            genre_dict = genres.set_index("genre")[["genreWeight"]].to_dict()

            dv_genre = DictVectorizer(sparse = SPARSE, dtype = int) #Depending on sklearns expected input, sparse may be activated to save memory and computing time
            dv_genre.fit_transform(genre_dict["genreWeight"])

            gen = genres.set_index("genre").groupby("movieID")[["genreWeight"]].apply(pd.DataFrame.to_dict, "dict").apply(lambda row: row['genreWeight']).apply(dv_genre.transform).apply(np.ravel)
            gen.name = "genre"
            genres = genres.drop("genreWeight", axis = 1)

            #movies = movies.join(gen)  #Value Error when executed twice
            return gen
    
def encode_directors(movies, directors, SPARSE = False):
    # Timon
    directors = directors.groupby(['directorID']).filter(lambda x: x['movieID'].count() > 5)

    if SPARSE == False:
        
        
        directors = directors.groupby(['directorID']).filter(lambda x: x['movieID'].count() > 5)
        base_enc = BaseNEncoder(cols = "directorID", base = 1)
        
        d = movies.merge(directors, on = "movieID", how = "left")
#       d_dummies = d[['movieID']].join(pd.get_dummies(d['directorID'])).groupby('movieID').max()
        d_dummies = d[['movieID']].join(base_enc.fit_transform(d["directorID"])).groupby('movieID').max()

        d_dummies['directors'] = d_dummies.values.astype("float32").tolist()
        d_dummies = d_dummies[['directors']]
        

        return d_dummies
    else: 
        
        directors["directorsWeight"] = 1
        directors = directors.groupby(['directorID']).filter(lambda x: x['movieID'].count() > 5)
        directors = pd.concat([directors,pd.DataFrame({"movieID": movies.index, "directorID": "_____", "directorsWeight": 0})])
        directors_dict = directors.set_index("directorID")[["directorsWeight"]].to_dict()
        dv_directors = DictVectorizer(sparse = SPARSE, dtype = int)
        dv_directors.fit_transform(directors_dict["directorsWeight"])
        
        dire = directors.set_index("directorID").groupby("movieID")[["directorsWeight"]].apply(pd.DataFrame.to_dict, "dict").apply(lambda row:row['directorsWeight']).apply(dv_directors.transform).apply(np.ravel)
        dire.name = "directors"
        
        return dire
    
def create_input(ratings, movies, encoder, SPARSE = False, encode_user = "label", encode_movieID = "label", years = False, standardscale = True, lda = True, cf_res = False,length = False, amount_of_imdbUserRatings= False, imdb_user_rating = False):
    start = dt.now()

    movies_ratings_merged = ratings
    #.merge(movies[["genre", "tags", "directors"]], on = "movieID")
    #Create Target Variable (Not Sparse)
    y = np.array(movies_ratings_merged[["rating"]]).ravel()
    
    #Create Empty Arrays, in case a value is 0
    X_user = np.array([]).reshape(ratings.shape[0], -1)
    X_movieID = np.array([]).reshape(ratings.shape[0], -1)

    X_genres = np.array([]).reshape(ratings.shape[0], -1)
    X_tags = np.array([]).reshape(ratings.shape[0], -1)
    X_movieID = np.array([]).reshape(ratings.shape[0], -1)
    X_directors = np.array([]).reshape(ratings.shape[0], -1)
    X_lda  = np.array([]).reshape(ratings.shape[0], -1)
    X_cf_pred = np.array([]).reshape(ratings.shape[0], -1)
    X_years = np.array([]).reshape(ratings.shape[0], -1)
    X_imdb_user_ratings = np.array([]).reshape(ratings.shape[0], -1)
    X_amount_of_imdbUserRatings = np.array([]).reshape(ratings.shape[0], -1)
    X_lengths = np.array([]).reshape(ratings.shape[0], -1)
    
    if SPARSE == False:
        if "genre" in movies.columns:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["genre"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_genres = np.concatenate(movies_ratings_merged["genre"].values).reshape(-1,len(movies_ratings_merged["genre"][0])).astype("float32")

            #if standardscale == True: 
            #    Sc = StandardScaler()
            #    X_genres = Sc.fit_transform(X_genres)
            print("Concatenated Genres, total time elapsed: ", (dt.now()-start))
        if "tags" in movies.columns: 
            movies_ratings_merged = movies_ratings_merged.merge(movies[["tags"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_tags = np.concatenate(movies_ratings_merged["tags"].values).reshape(-1,len(movies_ratings_merged["tags"][0])).astype("float32")   
            if standardscale == True: 
                Sc = StandardScaler()
                X_tags = Sc.fit_transform(X_tags)
            print("Concatenated Tags, total time elapsed: ", (dt.now()-start))

            
        if "directors" in movies.columns:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["directors"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_directors = np.concatenate(movies_ratings_merged["directors"].values).reshape(-1,len(movies_ratings_merged["directors"][0])).astype("float32") 
            print("Concatenated Directors, total time elapsed: ", (dt.now()-start))

            if standardscale == True: 
                Sc = StandardScaler()
                X_directors = Sc.fit_transform(X_directors)
                
        if "LDA_tags" in movies.columns:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["LDA_tags"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_lda = np.concatenate(movies_ratings_merged["LDA_tags"].values).reshape(-1,len(movies_ratings_merged["LDA_tags"][0])).astype("float32") 
            super_threshold_indices = X_lda < 0.01
            X_lda[super_threshold_indices] = 0
            print(X_lda)
            print("Concatenated LDA-Tags, total time elapsed: ", (dt.now()-start))
            #X_lda = X_lda /X_lda.mean(axis=0)
            if standardscale == True: 
                Sc = StandardScaler()
                X_lda = encoder.lda_scaler.transform(X_lda)
        if years == True:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["year"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_years = movies_ratings_merged[["year"]].values.tolist()
            if standardscale == True: 
                Sc = StandardScaler()
                X_years = encoder.year_scaler.transform(X_years)
            print("Concatenated Years, total time elapsed: ", (dt.now()-start))
            
        if length == True:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["length"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_lengths = movies_ratings_merged[["length"]].values.tolist()
            if standardscale == True: 
                Sc = StandardScaler()
                X_lengths = encoder.length_scaler.transform(X_lengths)
            print("Concatenated Lengths, total time elapsed: ", (dt.now()-start))
            
        if amount_of_imdbUserRatings == True:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["amount_of_imdbUserRatings"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_amount_of_imdbUserRatings = movies_ratings_merged[["amount_of_imdbUserRatings"]].values.tolist()
            if standardscale == True: 
                X_amount_of_imdbUserRatings = encoder.amount_imdb_ratings_scaler.transform(X_amount_of_imdbUserRatings)
            print("Concatenated amount_of_imdbUserRatings, total time elapsed: ", (dt.now()-start))
        
        if imdb_user_rating == True:
            movies_ratings_merged = movies_ratings_merged.merge(movies[["imdb_user_rating"]], on = "movieID").sort_values(by = ["userID", "movieID"])
            X_imdb_user_ratings = movies_ratings_merged[["imdb_user_rating"]].values.tolist()
            if standardscale == True: 
                X_imdb_user_ratings = encoder.imdb_ratings_scaler.transform(X_imdb_user_ratings)
            print("Concatenated imdb_user_ratings, total time elapsed: ", (dt.now()-start))
    
    
    
        if encode_user == "label":
            X_user = movies_ratings_merged[["userID"]].values.tolist()

        elif encode_user == "onehot":
            base_enc = BaseNEncoder(cols = "userID", base = 1) # Base = 1 equals OneHotencoding
            X_user = base_enc.fit_transform(movies_ratings_merged["userID"]).values.astype("float32").tolist()
            
        elif encode_user == "binary":
            base_enc = BaseNEncoder(cols = "userID", base = 2) # Base = 2 Equals Binary Encodeing
            X_user = base_enc.fit_transform(movies_ratings_merged["userID"]).values.astype("float32").tolist()
    
            
            #X_user = pd.get_dummies(movies_ratings_merged[["userID"]], columns = ["userID"]).values.tolist() 
        print("Encoded Users, total time elapsed: ", (dt.now()-start))
        
        if encode_movieID == "label":
            #label_enc = LabelEncoder()
            #X_user = label_enc.fit_transform(movies_ratings_merged["userID"]).reshape(1,-1).shape
            X_movieID = movies_ratings_merged[["movieID"]].values.tolist()

        elif encode_movieID == "onehot":
            base_enc = BaseNEncoder(cols = "movieID", base = 1) # Base = 1 equals OneHotencoding
            X_movieID = base_enc.fit_transform(movies_ratings_merged["movieID"]).values.astype("float32").tolist()
            
        elif encode_movieID == "binary":
            base_enc = BaseNEncoder(cols = "movieID", base = 2) # Base = 2 Equals Binary Encodeing
            X_movieID = base_enc.fit_transform(movies_ratings_merged["movieID"]).values.astype("float32").tolist()
            
        print("Encoded MovieIDs, total time elapsed: ", (dt.now()-start))
    
        #if lda:
        #    movies_ratings_merged = movies_ratings_merged.merge(movies[["LDA_tags"]], on = "movieID").sort_values(by=["userID", "movieID"])
        #    X_lda = np.concatenate(movies_ratings_merged["LDA_tags"].values).reshape(-1,len(movies_ratings_merged["LDA_tags"][0])).astype("float32")   
        #    if standardscale == True: 
        #        Sc = StandardScaler()
        #         X_lda = Sc.fit_transform(X_lda)
        #     print("Concatenated Tags, total time elapsed: ", (dt.now()-start))
            
        #Use Collaborative Filtering results if cf_res == True
        if cf_res ==True: 
            
            if not "cf_pred" in movies_ratings_merged.columns:
                cf_results = collaborative_filtering_NN(movies, ratings, epochs = 3, recommender = "v2")
                X_cf_pred = cf_results[0]

            
            if standardscale == True: 
                Sc = StandardScaler()
                X_cf_pred = Sc.fit_transform(X_cf_pred)
        
            movies_ratings_merged["cf_pred"] = X_cf_pred 
            
        #X_cf = movies_ratings_merged[['CFuserID', 'CFmovieID']].values   #TAGCLUSTERING
      #  movies_ratings_merged = movies_ratings_merged.merge(movies[["Cluster"]], on = "movieID").sort_values(by = ["CFuserID", "CFmovieID"])
       # X_cf = movies_ratings_merged[['CFuserID', 'Cluster']].values   #TAGCLUSTERING

    
        X = np.hstack((X_user, X_movieID,X_lda, X_genres, X_tags, X_directors, X_cf_pred, X_years, X_imdb_user_ratings, X_amount_of_imdbUserRatings, X_lengths))
            
    return X, y




def user_movie_rating_matrix(ratings):
    userRatings = ratings.pivot_table(index=['userID'],columns=['movieID'],values='rating')
    userRatings = userRatings.fillna(0,axis=1)
    return userRatings

def corr_movie_matrix(ratings):
    start = dt.now()
    user_ratings = user_movie_rating_matrix(ratings)
    Corr_Movie_Matrix = user_ratings.corr(method='pearson')
    print("Movie Correlation Matrix created: ", (dt.now()-start))
    return Corr_Movie_Matrix

# functions to access the Corr_Movie_Matrix
def get_similar(movie_name, Corr_Movie_Matrix,n):
    similar_ratings = Corr_Movie_Matrix[movie_name]
    return similar_ratings.nlargest(n)

def get_similar_rating(movie_name,rating, Corr_Movie_Matrix,n):
    similar_ratings = Corr_Movie_Matrix[movie_name]*(rating-2.5)
    return similar_ratings.nlargest(n)

# add k nearest movies independent of rating to "ratings_train"
# add k nearest movies independent of rating to "ratings_train"
def add_k_nearest_movies(ratings_train, Corr_Movie_Matrix,n):
    start = dt.now()
    user_ratings = user_movie_rating_matrix(ratings_train)
    similar_movies = []
    for column in Corr_Movie_Matrix.columns:
        movie_corr = []
        l = get_similar(column, Corr_Movie_Matrix,n).index.tolist()
        if l:
            l.pop(0)
        similar_movies.append(l)

    Similar_Movie_Matrix = Corr_Movie_Matrix.copy()
    Similar_Movie_Matrix['similar_movies'] = similar_movies
    Similar_Movie_Matrix = Similar_Movie_Matrix[['similar_movies']]
    Similar_Movie_Matrix

    ratings_train_similar_movies = ratings_train.merge(Similar_Movie_Matrix, on = 'movieID', how='left')
    print("Nearest Movies added: ", (dt.now()-start))

    return ratings_train_similar_movies

def add_ratings_similar_movies(ratings_train_similar_movies, n):
    start = dt.now()
    few_raters_rating = ratings_train_similar_movies.groupby('userID').filter(lambda x: x['rating'].count() < n)
    
    ratings_train_similar_movies_ext = pd.DataFrame(columns=['userID', 'movieID', 'rating', 'similar_movies'])

    for row in few_raters_rating.iterrows():
        for movies in row[1]['similar_movies']:
            new_row = {'userID':row[1]['userID'], 'movieID': movies, 'rating': row[1]['rating'], 'similar_movies': row[1]['similar_movies']}
            ratings_train_similar_movies_ext = ratings_train_similar_movies_ext.append(new_row, ignore_index=True)
    print("Ratings Extended: ", (dt.now()-start))
    return ratings_train_similar_movies_ext

def add_syn_ratings(ratings_train, k, n):
    #ratings_train = pd.DataFrame(x_users_train, columns = ["userID"])
    #ratings_train["movieID"] = x_movies_train
    #ratings_train["rating"] = y_train
    
    Corr_Movie_Matrix = corr_movie_matrix(ratings_train)

    ratings_train_similar_movies = add_k_nearest_movies(ratings_train, Corr_Movie_Matrix,k)
    ratings_train_similar_movies_ext = add_ratings_similar_movies(ratings_train_similar_movies,n)

    ratings_train_ext = ratings_train.append(ratings_train_similar_movies_ext[['userID', 'movieID', 'rating']])
    #ratings_train_ext['CFuserID'] = ratings_train_ext['userID']
    #ratings_train_ext['CFmovieID'] = ratings_train_ext['movieID']
    #ratings_train_ext['movieID'] = item_enc.inverse_transform(ratings_train_ext['CFmovieID'].values.astype(int))
    return ratings_train_ext


def encode_data(movies,ratings,movies_ratings_merged):
    encoder = Encoder()
    encoder.fit_LabelEncoder(ratings, movies)
    encoder.fit_StandardScaler(movies_ratings_merged)

    encoder.fit_act_tokenizer(movies)
    encoder.fit_plot_tokenizer(movies)

    # Save Encoder to call it in predict.py
    encoder.save()
    print("Saved_encoder")
    return encoder

def preprocess_inputs(ratings,movies,lda_data, ratings_train_ext, encoder, ratings_test, genres):
    print("CALLED The function")
    X_features_train, y_train = preprocessing_pipeline(ratings_train_ext, movies, encoder, standardscale = True,lda_data = lda_data, genres = genres, years = True, encode_user = "not",encode_movieID = "not", 
                           length = True, amount_of_imdbUserRatings = True, imdb_user_rating = True)
    

    X_features_test, y_test = preprocessing_pipeline(ratings_test, movies, encoder, standardscale = True, lda_data = lda_data, genres = genres, years = True, encode_user = "not",encode_movieID = "not", length = True, amount_of_imdbUserRatings = True, imdb_user_rating = True)

    
    return X_features_train, y_train, X_features_test, y_test



def prepare_inputs_predict(movies,ratings,lda_data, tags):

    tags = tags.rename({"id": "tagID"}, axis =1)

    movies = movies.rename({"id": "movieID"}, axis =1)
    movies = movies.set_index("movieID") 
    movies = movies[movies.index.isin(ratings.movieID)] 
    movies = add_actors(movies, actors)

    #movies = movies.join(lda_data.set_index("movieID")[["LDA_tags"]])

    ratings = ratings.rename({"user_id": "userID"}, axis =1)
    
    lda_data.LDA_tags = lda_data.LDA_tags.str.replace("[", "").str.replace("]", "").str.replace(",", "")
    lda_data.LDA_tags = lda_data.LDA_tags.apply(np.fromstring, dtype = float, sep = " ")

    movies = movies.join(lda_data.set_index("movieID")[["LDA_tags"]])
    movies = movies.join(directors.set_index("movieID")[["directorID"]])
    movies = movies.join(countries.set_index("movieID")[["country"]])
    movies["directorID"] = movies["directorID"].fillna("")
    movies["country"] = movies["country"].fillna("")

    movies_ratings_merged = ratings.merge(movies[["year", "amount_of_imdbUserRatings","imdb_user_rating", "directorID", "country", "length", "LDA_tags", "actorstring", "plot"]], on = "movieID").sort_values(by = ["userID"])
    


    # Add syntetic ratings to avoi the cold start problem
    ratings_train_ext = ratings_train
    #ratings_train_ext = add_syn_ratings(ratings_train, 2, 15)
    #ratings_train_ext = ratings_train_ext.sort_values(by = ["userID", "movieID"])

    return movies, ratings, tags, movies_ratings_merged, ratings_train_ext
