
## Project Organization
------------
```
	├── README.md 							
	├── __init__.py
	├── group_evaluation.py						
	├── .gitignore 						   
	├── data
	│   ├── modeling
	│   │   ├── dev 						
	│   │   ├── test 						
	│   │   │   └── test.csv 				
	│   │   └── train 					
	│   ├── preprocessed 				
	│   └── raw							
	│       ├── actors.csv
	│       ├── countries.csv
	│       ├── directors.csv
	│       ├── genres.csv
	│       ├── locations.csv
	│       ├── movie_tags.csv
	│       ├── movies.csv
	│       ├── ratings.csv
	│       └── tags.csv
	├── models														
	├── requirements.txt 					
	├── src
	│   ├── additional_features.py 			
	│   ├── predict.py 						
	│   ├── preprocessing.py 				
	│   └── train.py 						
	└── test.py 							
```